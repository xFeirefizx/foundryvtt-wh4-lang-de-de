# Translation: German [WFRP4]

This module will give you a German translation of the Foundry VTT Warhammer Fantasy Roleplay 4E system from Moo Man.

It aims to translate all text, including : 
 * Interface and sheets
 * Tables
 * Compendium

## Installation

Für die Installation sind folgende Schritte und weiteren Module nötig: 

1.  In Foundry VTT gehe in die Spieleinstellungen und dort unter "Manage Modules"
2.  Installiere ein Modul, das die Basis von Foundry VTT ins Deutsche übersetzt: 
3.  Installiere das Modul "babele"  : https://gitlab.com/riccisi/foundryvtt-babele/raw/master/module/module.json
4.  Installiere dieses Modul für die deutsche Übersetzung des WFRP 4E Moduls für Foundry VTT : https://gitlab.com/xFeirefizx/foundryvtt-wh4-lang-de-de/-/raw/master/module.json

## Compatibility

Foundry : 0.5.4+
Warhammer v4 : v1.2+
Babele : 1.9+

## Feedback

Feedback is more than welcome. Contact me "Feirefiz" in the Foundry VTT Discord server or here via a bug report.

## Acknowledgments

* Thanks to Moo Man for his great work in the module creation an design
* A big thanks also goes to LeRatierBretonnien who started the French translation for the WFRP 4E module for Foundry VTT

## License

Translation: German [WFRP4] is a module for Foundry VTT by xFeirefizx and is licensed under a [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).

This work is licensed under Foundry Virtual Tabletop [EULA - Limited License Agreement for module development v 0.1.6](http://foundryvtt.com/pages/license.html).
